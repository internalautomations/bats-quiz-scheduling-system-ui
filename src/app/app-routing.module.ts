import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScheduleComponent } from '../app/components/schedule/schedule.component'
import { AuthComponent } from '../app/components/auth/auth.component'
import { from } from 'rxjs';

const routes: Routes = [

  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {
    path: 'schedule',
    component: ScheduleComponent,
    data: {title: 'schedule'},
  // canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: AuthComponent,
    data: {title: 'Login'}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
