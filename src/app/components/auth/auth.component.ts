import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { Location } from '@angular/common'
import {
  FormControl,
  FormGroupDirective,
  FormBuilder,
  FormGroup,
  NgForm,
  Validators,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { AuthService } from 'src/app/services/auth-service';
import { StateService } from 'src/app/services/state.service';
import { Result } from 'src/app/struct/result';

/** Error when invalid control is dirty, touched, or submitted. 
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  } 
}*/


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  loginErr: boolean = false;
  regNumberErr: boolean = false;
  tokenErr: boolean = false;
  validRegNum: boolean = false;
  logInForm: FormGroup;
  username: string;
  password: string;

  isLoading = false;
  count: number = 0;
  registrationId: string = '';
  token: string = '';
  constructor(
    private router: Router,
    private api: ApiService,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private location: Location,
    private state: StateService
  ) { }

  ngOnInit(): void {
    this.tokenErr= false;
    this.logInForm = this.formBuilder.group({
      registrationId: [null, Validators.required],
      token: [null, Validators.required],
    });
  }

  generateToken() {    
    //console.log("Generate Token");
    this.registrationId = this.logInForm.value.registrationId;
    this.isLoading = true;
    this.api
      .auth(this.registrationId)
      .subscribe(
        (res: any) => {
          //console.log("Res: ", res);
          //console.log("Body: ", res.body);

          this.isLoading = false;
          if (res.body.status == "SUCCESS") {
            // localStorage.setItem('ACCESS_TOKEN', this.registrationId);
            // if (this.authService.isLoggedIn())
            //   this.router.navigateByUrl('/schedule');
            // else this.router.navigateByUrl('/login');
            //console.log('success case');
            
            this.validRegNum = true;
          } else {
            //console.log(res.body);
            //console.log('Failed');
            
            this.regNumberErr = true;
            this.loginErr = true;
          }
        },
        (err) => {
          alert('An unexpected error occured! Please try again later.')
          this.isLoading = false;
          //console.log(err);
        }
      );

    // if (this.logInForm.value.username)
    //   this.regNumberErr = false;
    // else
    //   this.regNumberErr = true;


    // this.regNumberErr = true
  }

  loginUser(){

    //console.log("Login");
    this.tokenErr= false;
    this.registrationId = this.logInForm.value.registrationId;
    this.token = this.logInForm.value.token;

    this.api
      .validateToken(this.registrationId, this.token)
      .subscribe(
        (res: any) => {
          //this.isLoading = false;
          //console.log("Result1: ->loginUser", res);
          //res.body = [''];
          if (res.body == 'SUCCESS') {
            
            localStorage.setItem('ACCESS_TOKEN', this.registrationId);
            if (this.authService.isLoggedIn()){
              this.state.regId = this.registrationId;
              this.state.token = this.token;
              this.router.navigateByUrl('/schedule');
            }
               
            else this.router.navigateByUrl('/login');
            this.validRegNum = true;
            this.regNumberErr = true;
          }  
          else {
            console.log('Else: ',res.body);
            this.count++;
            this.tokenErr= true;
            console.log('Failed');
            this.loginErr = true;
            if (this.count >= 3) {

              alert('LIMIT EXCEEDED! Please generate a new token.')
              
              //this.location
              this.count= 1;
              this.regNumberErr = false;
              this.validRegNum= false;
              this.tokenErr= false;
              this.router.navigate[('')];        
            }
          }
        },
        (err) => {
          this.count++;
          this.tokenErr= true;
          this.isLoading = false;
          //console.log(err);

          if (this.count >= 3) {

            alert('LIMIT EXCEEDED! Please generate a new token.')
            
            //this.location
            this.count= 1;
            this.regNumberErr = false;
            this.validRegNum= false;
            this.tokenErr= false;
            this.router.navigate[('')];        
          }
        }
      );

      

  }

  

}
