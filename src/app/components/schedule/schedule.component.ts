import { Component, OnInit } from '@angular/core';
import { DatePipe, formatDate } from '@angular/common';
import { ApiService } from '../../services/api.service'
import { stringify } from '@angular/compiler/src/util';
import { SlotDates } from 'src/app/struct/slot-dates';
import { findLast } from '@angular/compiler/src/directive_resolver';
import { StateService } from 'src/app/services/state.service';
​
@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css'],
  providers: [DatePipe]
})
export class ScheduleComponent implements OnInit {
  selectedDate ;
  selectedTime ;
  maxDate: String;
  minDate: String;
  myDate = new Date();
  errorMessage : String;
  disabledController : boolean = true;
  registrationId: string ='';
  token: string ='';
  slots;
  submitSuccess: boolean = false;
  submitFailure: boolean = false;
  startTime ;
  endTime;
  displayDate;
  failureMessage;
  alreadyBooked : boolean = false;
  noSlots : boolean = true;
  availableDates: SlotDates;
  constructor(private datePipe: DatePipe, private api: ApiService, private state: StateService ) { }
  ngOnInit(): void {
    this.registrationId = this.state.regId;
    this.token= this.state.token;
    this.getDates();
  
  }
  confirm(){
    if(this.selectedDate==undefined||this.selectedTime==undefined || this.selectedTime=="select"){
      alert("Select the fields!");
    }
    else {
      this.startTime = this.selectedTime.slice(0,8);
      this.endTime = this.selectedTime.slice(8,this.selectedTime.length);
      let confirmMessage = 'Confirm your test on '+formatDate(this.selectedDate,'MMM-dd-yyyy', 'en-US')+' from '+ this.startTime+' to '+this.endTime+'?'; 
      let r = confirm(confirmMessage);
      let data : Object = {
        date: formatDate(this.selectedDate, 'yyyy-MM-dd', 'en-US'),
        slot : { 
              startTime: this.startTime, 
              endTime: this.endTime
          }
      }
      console.log(data);
      if(r==true){
        this.api.scheduleTest(this.registrationId, this.token, data)
        .subscribe(
          (res: any) => {  
            if(res.status=="FAILURE"){
              this.submitFailure = true;
              this.failureMessage = res.message;
              if(this.failureMessage=='You have already booked a slot'){
                this.alreadyBooked = true;
              }
              else{
                this.alreadyBooked= false;
              }
            }
            else if(res=="SUCCESS"){
              this.submitSuccess = true;
              this.displayDate = formatDate(this.selectedDate, 'MMM-dd-yyyy', 'en-US')

            }
            // console.log('Response: ', res);       
          },
          (err) => {
            console.log(err);
            alert('An unexpected error occured! Please try again later.')
          }
        )
      }
  
    }
  }
  dateSelected(e){
    const dateFormat = 'yyyy-MM-dd';
    let formattedDate = formatDate(e.target.value, dateFormat, 'en-US');
    this.getSlots(formattedDate);
    
  }
  dateFilter = (d: Date) => {
    //return ([1, 5, 10, 21].indexOf(+d.getDate()) == -1);
    const day = (d || new Date()).getDay();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0 && day !== 6;
  };
​
  getDates(){
    //console.log('getDates: ',this.registrationId, this.token);
    
    this.api.getSlotDates(this.registrationId, this.token)
    .subscribe(
      (res: any) => {
       console.log('Response: ', res.body);        
       this.availableDates= res.body;
       this.minDate = stringify(res.body[0]); 
       this.maxDate = stringify(res.body[res.body.length-1]);       
      },
      (err) => {
        console.log(err);
      }
    );
  }
  getSlots(_date){
    this.api.getSlotTimes(this.registrationId, this.token , _date)
    .subscribe(
      (res) => {
        this.slots = res.body.slots;
        this.disabledController=false;
        if(res.body.slots.length<=0){
          // this.disabledController = true;
          this.noSlots = true;
        }
        else{
          this.noSlots=false;
        }
        // console.log('Response: ', res.body.slots.length);       
      },
      (err) => {
        console.log(err);
      }
    );
  }
  backToSelect(){
    this.submitFailure=false;
    const dateFormat = 'yyyy-MM-dd';
    // let formattedDate = formatDate(, dateFormat, 'en-US');
    this.getDates();
​
    this.getSlots(formatDate(this.selectedDate, dateFormat, 'en-US'));
  }
}