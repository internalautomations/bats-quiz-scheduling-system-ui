import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Result } from 'src/app/struct/result'
import { SlotDates } from 'src/app/struct/slot-dates'
import { SlotTimes } from 'src/app/struct/slot-times'
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpResponse,
} from '@angular/common/http';
​
​
@Injectable({
  providedIn: 'root'
})
export class ApiService {
​
  httpUrl = {
    dev: 'http://recruitment-system-internal-dev.us-e2.cloudhub.io/api',
    prod: 'http://recruitment-system-internal.us-e2.cloudhub.io/api',
    local : 'http://localhost:8081/api'
  };
  active = this.httpUrl.dev;
​
  constructor(private http: HttpClient) {}
​
  auth(registrationNumber: string): Observable<HttpResponse<any>> {
    //console.log("Service: ", registrationNumber);
    
    return this.http.get<any>(
      this.active+'/login/'+registrationNumber,
      {
        observe: 'response',
      
      }
    );
  }
​
  validateToken(registrationNumber: string, token: string): Observable<HttpResponse<any>>{
    
    return this.http.get<any>(
      this.active+'/validatetoken',
      {
        observe: 'response',
        headers: {
          registrationId: registrationNumber,
          token: token
        }
      
      }
    );
  }
​
  getSlotDates(registrationNumber: string, token: string): Observable<HttpResponse<SlotDates>>{
    // console.log('getSlotDates: ',registrationNumber + '         '+ token);
    return this.http.get<SlotDates>(
      this.active+'/slotdates',
      {
        observe: 'response',
        headers: {
          registrationId: registrationNumber,
          token: token
        }
      
      }
    );
  }
​
  getSlotTimes(registrationNumber: string, token: string, slotdate: string): Observable<HttpResponse<SlotTimes>>{
    return this.http.get<SlotTimes>(
      this.active+'/'+slotdate +'/slottimes',
      {
        observe: 'response',
        headers: {
          registrationId: registrationNumber,
          token: token
        }
      
      }
    );
  }
​
  scheduleTest(registrationNumber: string, token: string, slot: Object){
    //console.log(slot);
    
    return this.http.post<Object>(
      this.active+'/scheduletest',
      slot,
      {
        headers: {
          registrationId: registrationNumber,
          token: token
        }
      
      }
    );
  }
}



