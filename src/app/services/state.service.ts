import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class StateService {

  private message = new BehaviorSubject('First Message');
  sharedMessage = this.message.asObservable();

  regId: string='';
  token: string='';
  constructor() { }

  nextMessage(message: string) {
    this.message.next(message)
  }
}
