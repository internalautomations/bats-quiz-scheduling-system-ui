export class SlotTimes {
    date: string;
    slots: [
            {   startTime: string, 
                endTime: string
            }];
}
